﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TryCatch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double x = 0;
            double z = 0;
            try
            {
                x = Convert.ToDouble(textBox1.Text);
            }
            catch
            {
                MessageBox.Show("blogai ivestas pirmas skaicius", "Klaida");
                return;
            }
            try
            {

                z = Convert.ToDouble(textBox2.Text);
                
            }

            catch
            {
                MessageBox.Show("blogai ivestas antras skaicius", "Klaida");
                
            }
            textBox3.Text = (x + z).ToString();


        }
    }
}
